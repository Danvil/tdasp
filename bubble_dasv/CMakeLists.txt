add_definitions(-DSLIMAGE_IO_OPENCV)

link_directories(
	/home/david/Projects/dasp/release/libdasp
	/home/david/Projects/dasp/release/graphseg
)

add_library(bubble_dasv SHARED
	dasv.cpp
)

target_link_libraries(bubble_dasv
	dasp
	graphseg
	boost_thread
	opencv_core
	opencv_highgui
)

include_directories(
	${dasp_SOURCE_DIR}
	${dasp_SOURCE_DIR}/libdasp
	${dasp_SOURCE_DIR}/rgbd
)
