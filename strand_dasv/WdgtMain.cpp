#include "WdgtMain.h"
#include <graphseg/Rendering.hpp>
#include <common/color.hpp>
#include <Slimage/IO.hpp>
#include <boost/bind.hpp>

constexpr bool VIS_RENDER_GRAPH = true;

void PrepareEngine(const boost::shared_ptr<Candy::Engine>& engine)
{
	// auto view_ = engine->getView();
	auto scene = engine->getScene();

	// boost::shared_ptr<Candy::DirectionalLight> light1(new Candy::DirectionalLight(Danvil::ctLinAlg::Vec3f(+1.0f, +1.0f, -1.0f)));
	// light1->setDiffuse(Danvil::Colorf(1.0f, 1.0f, 1.0f));
	// scene->addLight(light1);
	// boost::shared_ptr<Candy::DirectionalLight> light2(new Candy::DirectionalLight(Danvil::ctLinAlg::Vec3f(-1.0f, -1.0f, -1.0f)));
	// light2->setDiffuse(Danvil::Colorf(1.0f, 1.0f, 1.0f));
	// scene->addLight(light2);

	engine->setClearColor(Danvil::Color::Grey);

	scene->setShowCoordinateCross(true);

}

WdgtMain::WdgtMain(bool no_gui, QWidget *parent)
    : QMainWindow(parent)
{
	ui.setupUi(this);

	no_gui_ = no_gui;

	enable_out_ = false;

	std::cout << "Preparing OpenGL widgets ..." << std::endl;

	// if(!no_gui) {
	// 	gui_dasp_params_.reset(new WdgtDaspParameters(strands_->dasp_opt_));
	// 	gui_dasp_params_->setAttribute(Qt::WA_DeleteOnClose, false);
	// 	gui_dasp_params_->reload = &dasp_parameters_changed_;
	// 	gui_dasp_params_->show();
	// }

	if(!no_gui) {
		widget_candy_ = new Candy::GLSystemQtWindow(0);
		widget_candy_->setAutoUpdateInterval(20);
		ui.widget->addCustomWidget(widget_candy_, "3D global");
		engine_ = widget_candy_->getEngine();
		PrepareEngine(engine_);

		{
			boost::shared_ptr<Candy::IRenderable> renderling(new Candy::ObjectRenderling(
				[this]() {
					this->renderStrandGraph();
				}
			));
			engine_->getScene()->addItem(renderling);
		}
	}

}

WdgtMain::~WdgtMain()
{
}

void WdgtMain::setStrands(const std::shared_ptr<strand_dasv::StrandDasvControlerImpl>& strands)
{
	strands_ = strands;
	if(!no_gui_) {
		//strands_->image_callback_ = boost::bind(&WdgtMain::showImageThreadsafe, this, _1, _2);
		strands_->setPlotFunction(
			std::bind(&WdgtMain::updateImage, this, std::placeholders::_1, std::placeholders::_2));
	}
}

void WdgtMain::updateImage(const std::string& tag, const slimage::Image3ub& img)
{
	if(enable_out_) {
		slimage::Save(img, (out_fmt_ % current_frame_).str() + tag + ".png");
	}
	QImage* qimg = slimage::qt::ConvertToQt(img);
	ui.widget->miv()->set(tag, *qimg);
	delete qimg;
}

void WdgtMain::closeEvent(QCloseEvent* event)
{
	gui_dasp_params_->close();
	event->accept();
}

void WdgtMain::setRgbdStream(const std::shared_ptr<RgbdStream>& rgbd_stream)
{
	rgbd_stream_ = rgbd_stream;
	std::shared_ptr<RandomAccessRgbdStream> ra_rgbd_stream
		= std::dynamic_pointer_cast<RandomAccessRgbdStream>(rgbd_stream);

	worker_interupt_ = true;
	if(worker_.joinable())
		worker_.join();

	worker_ = std::thread(
		[this, ra_rgbd_stream]() {
			worker_interupt_ = false;
			strands_->setRgbdFunction([rgbd_stream_, ra_rgbd_stream](unsigned int i) {
				if(ra_rgbd_stream) {
					ra_rgbd_stream->seek(i);
				}
				rgbd_stream_->grab();
				return rgbd_stream_->get();
			});
			unsigned int num_frames = 0;
			if(ra_rgbd_stream) {
				num_frames = ra_rgbd_stream->numFrames();
			}
			for(unsigned int i=0; i<num_frames || num_frames == 0; ++i) {
				current_frame_ = i;
				strand_dasv::data_ptr x = strands_->request(i);
				{
					std::lock_guard<std::mutex> lock(dasv_graph_mutex_);
					strand_graph_ = x->strand_graph;
				}
				if(worker_interupt_) {
					break;
				}
			}
			if(!widget_candy_) { // FIXME hack to check if we are in nogui mode
				exit(0);
			}
		});

}

void WdgtMain::setTag(const std::string& tag)
{
	enable_out_ = true;
	out_fmt_ = boost::format(tag + "%05d");
//	strands_->tag_ = tag;
}

Eigen::Vector3f GetPosition(const dasp::Point& p, int t)
{
	return Eigen::Vector3f{
						0.01f*static_cast<float>(p.px),
						0.01f*static_cast<float>(p.py),
						0.03f*static_cast<float>(t)
					};
}

void WdgtMain::renderStrandGraph()
{
	if(!VIS_RENDER_GRAPH) {
		return;
	}
	if(!strands_) {
		return;
	}
	strand_dasv::StrandGraph graph;
	{
		std::lock_guard<std::mutex> lock(dasv_graph_mutex_);
		graph = strand_graph_;
	}
//	// std::cout << "Rendering graph with " << boost::num_vertices(graph) << " nodes" << std::endl;
	// render strands
	int current_time = 0;
	for(const auto& vid : as_range(boost::vertices(graph))) {
		const strand_dasv::Strand& strand = graph[vid];
		current_time = std::max<int>(current_time, strand.end_time());
	}
	glLineWidth(6.0f);
	for(const auto& vid : as_range(boost::vertices(graph))) {
		const strand_dasv::Strand& strand = graph[vid];
//			std::cout << "Strand with " << graph[vid].centers.size() << " pnts" << std::endl;
		// render strand
		glBegin(GL_LINE_STRIP);
		graphseg::detail::GlColor(
			strand.centers.back().color
			//common::CountColor(strand.length(), 1, 150)
		);
		int t = strand.end_time() - 1;
		for(auto it=strand.centers.rbegin(); it!=strand.centers.rend(); ++it) {
			const dasp::Point& p = *it;
			// graphseg::detail::GlColor(p.color);
			graphseg::detail::GlVertex(
				GetPosition(p, current_time-t));
			--t;
			if(current_time - t >= 60)  {
				break;
			}
		}
		glEnd();
	}
// 	glLineWidth(1.0f);
// 	for(const auto& eid : as_range(boost::edges(graph))) {
// 		auto vid1 = boost::source(eid, graph);
// 		auto vid2 = boost::target(eid, graph);
// 		glBegin(GL_LINES);
// 		for(auto p : graph[eid].all_weights) {
// 			const int t = p.first;
// 			const float weight = p.second;
// 			const auto& s1 = graph[vid1];
// 			const auto& s2 = graph[vid2];
// 			graphseg::detail::GlColor(
// 				common::IntensityColor(weight));
// 			graphseg::detail::GlVertex(
// 				GetPosition(s1.centers[t - s1.start_time], current_time-t));
// 			graphseg::detail::GlVertex(
// 				GetPosition(s2.centers[t - s2.start_time], current_time-t));
// 		}
// 		glEnd();
// 	}
	// render current strand graph
	glLineWidth(4.0f);
	graphseg::RenderEdges3D(graph,
		[&graph](const strand_dasv::StrandGraph::vertex_descriptor& vid) {
			// returns vertex coordinate
			const dasp::Point& p = graph[vid].centers.back();
			return Eigen::Vector3f{
				0.01f*static_cast<float>(p.px),
				0.01f*static_cast<float>(p.py),
				0.0f
			};
			// return p.position;
		},
		[&graph](const strand_dasv::StrandGraph::edge_descriptor& eid) {
			return common::SimilarityColor(graph[eid].weight);
		}
	);
}
