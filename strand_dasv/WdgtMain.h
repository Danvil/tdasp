#ifndef DASPSTRANDS_WDGTMAIN_H
#define DASPSTRANDS_WDGTMAIN_H

#include "ui_WdgtMain.h"
#include "strand_dasv_controler.hpp"
#include <rgbd.hpp>
#include <graphseg/graphseg.hpp>
#include <common/WdgtDaspParameters.h>
#include <Candy/System/GLSystemQtWindow.h>
#include <Candy.h>
#include <Slimage/Slimage.hpp>
#include <miv/WdgtMdiView.h>
#include <QtGui/QMainWindow>
#include <boost/format.hpp>
#include <thread>
#include <mutex>
#include <string>

class WdgtMain : public QMainWindow
{
    Q_OBJECT

public:
	WdgtMain(bool no_gui, QWidget *parent = 0);
	~WdgtMain();

protected:
	void closeEvent(QCloseEvent* event);

public:
	void setStrands(const std::shared_ptr<strand_dasv::StrandDasvControlerImpl>& strands);
	void setRgbdStream(const std::shared_ptr<RgbdStream>& rgbd_stream);

	void setTag(const std::string& tag);

private:
	void updateImage(const std::string& tag, const slimage::Image3ub& img);
	void renderStrandGraph();

private:
	bool no_gui_;
	bool enable_out_;
	boost::format out_fmt_;

	Candy::GLSystemQtWindow* widget_candy_;
	boost::shared_ptr<Candy::Engine> engine_;

	boost::shared_ptr<WdgtDaspParameters> gui_dasp_params_;
	bool dasp_parameters_changed_;

	std::shared_ptr<RgbdStream> rgbd_stream_;

	std::shared_ptr<strand_dasv::StrandDasvControlerImpl> strands_;

	std::thread worker_;
	bool worker_interupt_;
	unsigned int current_frame_;

	strand_dasv::StrandGraph strand_graph_;
	std::mutex dasv_graph_mutex_;

private:
    Ui::WdgtMainClass ui;
};

#endif
