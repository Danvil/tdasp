#imagedir="/tmp/strands"
imagedir="$1"

olddir=(`pwd`)
cd "$imagedir"

imgArr=(`ls *color.png`)
imgcount=${#imgArr[*]}
echo $imgcount

imgColor=(`ls *color.png`)
imgDepth=(`ls *depth.png`)
imgDasp=(`ls *dasp.png`)
imgLabels=(`ls *segs_labels.png`)

crop="[520x390+60+45]"
geom="-geometry 520x390"

for ((i=0;i<$imgcount;i=$i+1)); do
	echo "Montaging $i"
	montage ${imgColor[$i]}${crop} ${imgDepth[$i]}${crop} ${imgDasp[$i]}${crop} ${imgLabels[$i]}${crop} ${geom} $(printf m_%05d.png ${i})
done

echo "Creating video..."
ffmpeg -i m_%05d.png -vcodec libx264 m.mp4

cd "$olddir"
