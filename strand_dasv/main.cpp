#include "WdgtMain.h"
#include "strand_dasv_controler.hpp"
#include "strand_dasv.hpp"
#include <QtGui>
#include <QApplication>
#include <boost/program_options.hpp>
#include <iostream>
#include <string>

int main(int argc, char *argv[])
{
	namespace po = boost::program_options;

	bool p_nogui = false;
	std::string p_rgbd_mode = "test";
	std::string p_rgbd_arg = "uniform";
	unsigned int p_rgbd_seek = 0;
	std::string p_tag = "";

	strand_dasv::Parameters params;

	// parse command line options
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("nogui", po::value(&p_nogui)->default_value(p_nogui), "set to true to hide gui")
		("rgbd_mode", po::value(&p_rgbd_mode)->default_value(p_rgbd_mode), "rgbd stream mode (test, static, oni, live)")
		("rgbd_arg", po::value(&p_rgbd_arg)->default_value(p_rgbd_arg), "rgbd stream argument")
		("rgbd_seek", po::value(&p_rgbd_seek)->default_value(p_rgbd_seek), "rgbd stream seek (only usable with mode=oni)")
		("tag", po::value(&p_tag)->default_value(p_tag), "path/tag for output file names")
		("dasp.radius", po::value(&params.dasp.base_radius)->default_value(params.dasp.base_radius), "dasp.radius")
		("dasp.num", po::value(&params.dasp.count)->default_value(params.dasp.count), "dasp.num")
		("dasp.smooth_density", po::value(&params.dasp.smooth_density)->default_value(params.dasp.smooth_density), "dasp.smooth_density")
		("dasp.iterations", po::value(&params.dasp.iterations)->default_value(params.dasp.iterations), "dasp.iterations")
		("sg.num", po::value(&params.strand_graph.max_num_vertices)->default_value(params.strand_graph.max_num_vertices), "sg.num")
		("ucm.t", po::value(&params.ucm.t)->default_value(params.ucm.t), "ucm.t")
		("ucm.t_join", po::value(&params.ucm.t_join)->default_value(params.ucm.t_join), "ucm.t_join")
		("ucm.t_separate", po::value(&params.ucm.t_separate)->default_value(params.ucm.t_separate), "ucm.t_separate")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);    

	if (vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	std::cout << "Creating widget..." << std::endl;
	QApplication a(argc, argv);
	WdgtMain w(p_nogui);
	if(!p_nogui)
		w.show();

	std::cout << "Settings strand parameters..." << std::endl;
	auto strands = std::make_shared<strand_dasv::StrandDasvControlerImpl>();
	strands->params = params;
	w.setStrands(strands);

	std::cout << "Opening RGBD stream..." << std::endl;
	std::shared_ptr<RgbdStream> rgbd_stream = FactorStream(p_rgbd_mode, p_rgbd_arg);
	if(p_rgbd_seek != 0) {
		auto rgbd_stream_ra = std::dynamic_pointer_cast<RandomAccessRgbdStream>(rgbd_stream);
		if(rgbd_stream_ra) {
			rgbd_stream_ra->seek(p_rgbd_seek);
		}
	}
	w.setRgbdStream(rgbd_stream);

	if(!p_tag.empty()) {
		w.setTag(p_tag);
	}

	return a.exec();
}
