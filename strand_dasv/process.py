import subprocess
import os

dasp_aux_proj_dir = '/home/david/Projects/dasp_aux/release'
db_dir = '/home/david/Documents/DataSets/2013-01_DASV'
result_dir = '/home/david/Desktop/dasv/'
dryrun = True

def execute(cmds, num_threads):
	''' Executes system commands in parallel using up to num_threads '''
	processes = set()
	for cmd in cmds:
		print 'Executing command:', ' '.join(cmd)
		if dryrun: continue
		processes.add(subprocess.Popen(cmd))
		if len(processes) >= num_threads:
			os.wait()
			processes.difference_update(
				p for p in processes if p.poll() is not None)
	for p in processes:
		if p.poll() is None:
			p.wait()

def task_toimages_cmd(fn_oni, dst):
	if not os.path.exists(dst):
		os.makedirs(dst)
	return [
		dasp_aux_proj_dir + '/stream_to_images/stream_to_images',
		'--rgbd_m', 'oni',
		'--rgbd_a', fn_oni,
		'--rgbd_s', '5',
		'--tag', dst
	]

def task_toimages(tags):
	cmds = [];
	for tag in tags:
		cmd = task_toimages_cmd(
			db_dir + '/' + tag + '.oni',
			result_dir + '/images/' + tag + '/',
		)
		cmds.append(cmd)
	return cmds

def task_dasv_cmd(tag, dst, num, t):
	if not os.path.exists(dst):
		os.makedirs(dst)
	return [
		dasp_aux_proj_dir + '/dasp_strands/dasp_strands',
		'--rgbd_m', 'oni',
		'--rgbd_a', db_dir + '/' + tag + '.oni',
		'--rgbd_s', '5',
		'--tag', dst,
		'--num', str(num),
		'--edge_threshold', str(t),
		'--nogui', '1'
	]

def task_dasv_cmd_images(tag, dst, num, t):
	if not os.path.exists(dst):
		os.makedirs(dst)
	return [
		dasp_aux_proj_dir + '/dasp_strands/dasp_strands',
		'--rgbd_m', 'images',
		'--rgbd_a', result_dir + '/images/' + tag + '/',
		'--tag', dst,
		'--num', str(num),
		'--edge_threshold', str(t),
		'--nogui', '1'
	]

def task_dasv(tags, nums, ts):
	cmds = []
	for tag in tags:
		for num in nums:
			for t in ts:
				cmd = task_dasv_cmd(
					tag,
					result_dir + '/' + str(num) + '-' + str(t) + '/' + tag + '/',
					num,
					t
				)
				cmds.append(cmd)
	return cmds

def task_evaluate_cmd(dl, dc, dd, out, num):
	return [
		dasp_aux_proj_dir + '/eval_dasv/eval_dasv',
		'--dl', dl,
		'--dc', dc,
		'--dd', dd,
		'--out', out,
		'--num', str(num),
		'--sgbh', '0'
	]

def count_files(dir):
	return len([name for name in os.listdir(dir) if os.path.isfile(dir + '/' + name)])

def task_evaluate(tags, nums, ts):
	cmds = []
	for tag in tags:
		for num in nums:
			for t in ts:
				cnt = count_files(result_dir + '/images/' + tag) / 2
				sub = str(num) + '-' + str(t)
				cmds.append(task_evaluate_cmd(
					result_dir + '/' + sub + '/' + tag,
					result_dir + '/images/' + tag,
					result_dir + '/images/' + tag,
					result_dir + '/' + sub + '-' + tag + '.tsv',
					cnt
				))
	return cmds

v_onis1 = [
	 'Alexander_coffee2'
	,'Alexander_rotatingcam1'
	,'ball-move-mixed-2'
	,'box-double-1'
	,'Person-Hand-4'
]

v_onis2 = [
	 'Alexander_water'
	,'Alexander_coffee'
	,'Background'
	,'ball-move'
	,'box-1'
	,'box-2'
	,'box-3'
	,'box-double-2'
	,'box-double-3'
	,'Person-Hand-1'
	,'Person-Hand-2'
	,'Person-Hand-3'
	,'Person-Hand-5'
	,'Person-Hand-6'
	,'ball-move-mixed-1'
	,'ball-move-mixed-3'
	,'ball-move-mixed-4'
	,'ball-move-mixed-5'
]

dryrun = False
num_threads = 5

print 'Preparing commands...'

#cmds = task_toimages(v_onis1)

#cmds = task_dasv(v_onis1, [600, 800, 1000, 1200], [1.0, 2.0, 3.0, 5.0])
#cmds = task_dasv(v_onis1, [800,1000,1200], [2.0])

cmds = task_dasv(
	['Alexander_coffee2','Alexander_rotatingcam1','box-double-1'],
	[1000], [2.3])

#cmds = task_evaluate(v_onis1, [800,1000,1200], [2.0])
#cmds = task_evaluate(v_onis1, [1000], [5.0])

print 'Executing', len(cmds), 'commands...'
execute(cmds, num_threads)
print 'Finished.'
