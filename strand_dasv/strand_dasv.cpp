#include "strand_dasv.hpp"
#include <common/color.hpp>
#include <dasp/Neighbourhood.hpp>
#include <dasp/Segmentation.hpp>
#include <dasp/Metric.hpp>
#include <dasp/Plots.hpp>
#include <graphseg/graphseg.hpp>
#include <graphseg/Labeling.hpp>
#include <fstream>
#include <random>
#include <map>

namespace strand_dasv {

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

DaspParameters::DaspParameters()
: count(0),
  base_radius(0.06f),
  iterations(5),
  weight_spatial(1.0f),
  weight_color(3.0f),
  weight_normal(2.0f),
  smooth_density(false)
{}

dasp::Parameters dasp_get_params(const DaspParameters& params)
{
	dasp::Parameters dasp_opt;
	dasp_opt.seed_mode = dasp::SeedModes::Delta;
	dasp_opt.count = 0;
	dasp_opt.base_radius = 0.06f;
	dasp_opt.iterations = params.iterations;
	dasp_opt.weight_spatial = params.weight_spatial;
	dasp_opt.weight_color = params.weight_color;
	dasp_opt.weight_normal = params.weight_normal;
	dasp_opt.is_repair_depth = false;
	dasp_opt.enable_clipping = false;
	dasp_opt.is_smooth_density = params.smooth_density;
	return dasp_opt;
}

dasp::Superpixels dasp_init(const Rgbd& rgbd, DaspParameters& params)
{
	dasp::Superpixels clustering;
	clustering.opt = dasp_get_params(params);
	clustering.opt.seed_mode = dasp::SeedModes::Delta;
	clustering.opt.count = params.count;
	dasp::ComputeSuperpixelsIncremental(clustering, rgbd.color, rgbd.depth);
	params.base_radius = clustering.opt.base_radius;
	return clustering;
}

void dasp(const Rgbd& rgbd, dasp::Superpixels& clustering, const DaspParameters& params)
{
	clustering.opt = dasp_get_params(params);
	clustering.opt.base_radius = params.base_radius;
	for(unsigned int i=0; i<clustering.cluster.size(); i++) {
		clustering.cluster[i].seed_id = i;
	}
	dasp::ComputeSuperpixelsIncremental(clustering, rgbd.color, rgbd.depth);
}

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

DaspGraphParameters::DaspGraphParameters()
: weight_spatial(1.0f),
  weight_color(5.0f),
  weight_normal(4.0f)
{}

struct ColorGradientSuperpixelSimiliarity
{
	static constexpr float cStdDevMain = 0.01f*0.01f;

	struct Gaussian3
	{
		void build(const Eigen::Vector3f& mean, const std::vector<Eigen::Vector3f>& v) {
			mean_ = mean;
			Eigen::Matrix3f q = Eigen::Matrix3f::Zero();
			for(const auto& x : v) {
				auto d = x - mean_;
				q += d * d.transpose();
			}
			q /= static_cast<float>(v.size());
			q += cStdDevMain * Eigen::Matrix3f::Identity();
			var_inv_ = q.inverse();
			assert(std::isnan(var_inv_(0,0)));
			scale_ = 1.0f / std::sqrt(std::pow(2.0f*3.1415f,3.0f)*q.determinant());
		}

		float mahalanobis(const Eigen::Vector3f& x) const {
			auto d = x - mean_;
			return d.dot(var_inv_*d);
		}

		float operator()(const Eigen::Vector3f& x) const {
			return std::exp(-0.5f*mahalanobis(x));
		}

	private:
		Eigen::Vector3f mean_;
		Eigen::Matrix3f var_inv_;
		float scale_;
	};

	ColorGradientSuperpixelSimiliarity(const dasp::Superpixels& superpixels, float base_radius)
	: superpixels_(superpixels), base_radius_(base_radius) {
		// compute superpixel color distribution
		color_dist_.resize(superpixels_.cluster.size());
		for(unsigned int i=0; i<color_dist_.size(); ++i) {
			const auto& c = superpixels.cluster[i];
			std::vector<Eigen::Vector3f> data;
			data.reserve(c.pixel_ids.size());
			for(unsigned int j : c.pixel_ids) {
				data.push_back(superpixels_.points[j].color);
			}
			color_dist_[i].build(c.center.color, data);
		}
		// // DEBUG
		// std::ofstream ofs("/tmp/colordist.tsv");
		// for(unsigned int i=0; i<superpixels_.cluster.size(); ++i) {
		// 	const auto& c = superpixels_.cluster[i];
		// 	for(unsigned int j : c.pixel_ids) {
		// 		const auto& p = superpixels_.points[j];
		// 		ofs << i << "\t" << p.color.transpose() << std::endl;
		// 	}
		// }
	}

	float operator()(unsigned int i1, unsigned int i2) const {
		const dasp::Point& cc1 = superpixels_.cluster[i1].center;
		const dasp::Point& cc2 = superpixels_.cluster[i2].center;
		// spatial
		float dsh = (cc1.position - cc2.position).squaredNorm() / (4.0f*base_radius_ * base_radius_);
		float ds = std::max(0.0f, dsh - 1.2f);
		// color
		const Gaussian3& dist_1 = color_dist_[i1];
		const Gaussian3& dist_2 = color_dist_[i2];
		float dc1 = dist_2.mahalanobis(cc1.color);
		float dc2 = dist_1.mahalanobis(cc2.color);
		float dc = 0.5f*(dc1 + dc2);
		// total
		float d = 0.33f*ds + 0.67f*dc;
		return std::exp(-0.5*d);
	}

private:
	const dasp::Superpixels& superpixels_;
	float base_radius_;
	std::vector<Gaussian3> color_dist_;
};

std::pair<DaspGraph, dasp::NeighbourhoodGraph> dasp_graph(
	const dasp::Superpixels& clustering,
	const DaspGraphParameters& params)
{
	const unsigned int num_vertices = clustering.clusterCount();
	// compute neighbourhood graph
	auto g_nb = dasp::CreateNeighborhoodGraph(clustering,
		dasp::NeighborGraphSettings::NoCut());

	// compute weights for neighbourhood graph
	dasp::UndirectedWeightedGraph g_nb_w = ComputeEdgeWeights(clustering, g_nb,
		dasp::ClassicSpectralAffinity<true>(
			num_vertices,
			clustering.opt.base_radius,
			params.weight_spatial,
			params.weight_color,
			params.weight_normal));

	// // compute weights for neighbourhood graph (new gradient method)
	// dasp::UndirectedWeightedGraph g_nb_w = ComputeEdgeWeightsFromMetricWeighted(clustering, g_nb,
	// 	ColorGradientSuperpixelSimiliarity(clustering, clustering.opt.base_radius));

	// create dasp graph
	DaspGraph g(num_vertices);
	// copy vertices from dasp clusters
	for(unsigned int i=0; i<num_vertices; i++) {
		DaspVertex& v = g[i];
		const dasp::Cluster& c = clustering.cluster[i];
		v.point = c.center;
		v.seed_id = c.seed_id;
	}
	// copy edge weights from weighted neighbourhood graph
	for(auto eid : as_range(boost::edges(g_nb_w))) {
		auto p = boost::add_edge(
			boost::source(eid, g_nb_w),
			boost::target(eid, g_nb_w),
			g
		);
		assert(p.second);
		g[p.first] = g_nb_w[eid];
	}
	return {g, g_nb};
}

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

StrandGraphParameters::StrandGraphParameters()
: color_threshold(0.12f),
  depth_threshold(0.06f),
  edge_weight_decay(0.05f),
  vertex_mix(0.5f),
  edge_weight_mix(0.5f),
  max_num_vertices(1500)
{}

bool can_continue_strand(const dasp::Point& a, const dasp::Point& b, const StrandGraphParameters& params)
{
	const float dc = (a.color - b.color).norm();
	const float dd = std::abs(a.position[2] - b.position[2]);
	// const float dx = (a.position - b.position).norm();
	// const float dn = (1.0f - a.normal.dot(b.normal));
	bool will_it_break = 
		dc >= params.color_threshold
		|| dd >= params.depth_threshold
		// && dx < STRAND_POSITION_THRESHOLD
		// && dn < STRAND_NORMAL_THRESHOLD
		;
	return !will_it_break;
}

std::vector<StrandGraph::vertex_descriptor> dasp_to_strand_graph(StrandGraph& strand_graph, const DaspGraph& dasp_graph, unsigned int t,
	const StrandGraphParameters& params
) {
	static unsigned int s_next_strand_id = 0;

	// diffusion
	for(auto eid : as_range(boost::edges(strand_graph))) {
		strand_graph[eid].weight =
			(1.0f - params.edge_weight_decay) * strand_graph[eid].weight
			+ params.edge_weight_decay * 0.0f; // decay to zero (=unconnected)
	}

	// last_superpixel_posid saves the positional id of the dasp which is at the end of the strand
	// or -1 if the strand is not active

	// create label to vertex id map and find index for new labels
	std::map<int,int> dasp_posid_to_strand_vid;
	for(auto vid : as_range(boost::vertices(strand_graph))) {
		int& dasp_posid = strand_graph[vid].last_superpixel_posid;
		if(dasp_posid != -1) {
			dasp_posid_to_strand_vid[dasp_posid] = vid;
		}
		// set to -1 (will be reset later)
		dasp_posid = -1;
	}

	std::vector<StrandGraph::vertex_descriptor> dasp_to_strand(boost::num_vertices(dasp_graph));

	// adding new clusters to graph and assign label
	for(auto gvid : as_range(boost::vertices(dasp_graph))) {
		const dasp::Point& center_pnt = dasp_graph[gvid].point;
		int dasp_posid = dasp_graph[gvid].seed_id;
		// find label and graph vertex id
		int strand_vid = -1;
		bool start_new = true;
		if(dasp_posid != -1) {
			// find the supervoxel which gave the seed
			auto it = dasp_posid_to_strand_vid.find(dasp_posid);
			// every dasp seed_id must be present!
			assert(it != dasp_posid_to_strand_vid.end());
			strand_vid = it->second;
			// check if cluster can be connected to strand
			if(can_continue_strand(strand_graph[strand_vid].running_mean, center_pnt, params)) {
				// continue strand
				start_new = false;
			}
		}
		// start a new strand
		if(start_new) {
			strand_vid = boost::num_vertices(strand_graph);
			Strand s;
			s.last_superpixel_posid = -1;
			s.strand_id = s_next_strand_id++;
			s.segment_label = (dasp_posid != -1 ? -2 /* strand broke */ : -1 /* new cluster */);
			s.start_time = t;
			s.running_mean = center_pnt;
			boost::add_vertex(s, strand_graph);
		}
		// add cluster to strand
		dasp_to_strand[gvid] = strand_vid;
		Strand& s = strand_graph[strand_vid];
		s.last_superpixel_posid = gvid; // dasp_graph vertex id is superpixel position in vector
		s.centers.push_back(center_pnt);
		// mix in running mean
		s.running_mean.color = params.vertex_mix * s.running_mean.color + (1.0f - params.vertex_mix) * s.centers.back().color;
		// s.running_mean.position = STRAND_RUNNING_MEAN_ALPHA * s.running_mean.position + (1.0f - STRAND_RUNNING_MEAN_ALPHA) * s.centers.back().position;
		s.running_mean.position = s.centers.back().position;
		// s.running_mean.normal = STRAND_RUNNING_MEAN_ALPHA * s.running_mean.normal + (1.0f - STRAND_RUNNING_MEAN_ALPHA) * s.centers.back().normal;
		s.running_mean.normal = s.centers.back().normal;
	}

	// merge current neighbourhood graph with strand graph 
	for(auto nvid : as_range(boost::vertices(dasp_graph))) {
		auto svid = dasp_to_strand[nvid];
		// create edges and update edge weight
		for(auto eid : as_range(boost::out_edges(nvid, dasp_graph))) {
			auto target_nvid = boost::target(eid, dasp_graph);
			auto target_svid = dasp_to_strand[target_nvid];
			float nweight = dasp_graph[eid];
			// create edge between strands if it does not exists
			StrandGraph::edge_descriptor seid;
			bool ok;
			boost::tie(seid, ok) = boost::edge(svid, target_svid, strand_graph);
			if(!ok) {
				boost::tie(seid, ok) = boost::add_edge(svid, target_svid, strand_graph);
				strand_graph[seid].weight = nweight; //0.0f;
			}
			assert(ok);
			// store current dasp graph edge weight
			auto& s = strand_graph[seid];
			s.all_weights[t] = nweight;
			// accumulate dasv graph edge weight using current dasp graph edge weight
			s.weight =
				(1.0f - params.edge_weight_mix) * s.weight
				+ params.edge_weight_mix * nweight;
			// const unsigned int num1 = strand_graph_[svid].centers.size();
			// const unsigned int num2 = strand_graph_[target_svid].centers.size();
			// const unsigned int rel_cnt = std::min(num1, num2);
			// s.weight = std::accumulate(
			// 	s.all_weights.begin(), 	s.all_weights.end(),
			// 	0.0f,
			// 	[](float a, const std::pair<int,float>& p) {
			// 		return a + p.second;
			// 	}) / static_cast<float>(rel_cnt);
		}
	}

	// clean old strands from graph to reduce size
	if(params.max_num_vertices >= 0) {
		// deleting old strands
		// get a list of vertex indices
		std::vector<int> vert_indices(boost::num_vertices(strand_graph));
		std::iota(vert_indices.begin(), vert_indices.end(), 0);
		// sort such that oldest come first
		std::sort(vert_indices.begin(), vert_indices.end(),
			[&strand_graph](int x, int y) {
				return strand_graph[x].end_time() < strand_graph[y].end_time();
			}
		);
		// erase those we want to keep (biggest end_time)
		unsigned int keep_time = t + 1;
		auto it = std::find_if(vert_indices.begin(), vert_indices.end(),
			[&strand_graph,keep_time](int x) {
				return strand_graph[x].end_time() == keep_time;
			}
		);
		int num_keep = std::max<int>(0, static_cast<int>(params.max_num_vertices) - static_cast<int>(std::distance(it, vert_indices.end())));
		vert_indices.erase(it, vert_indices.end());
		vert_indices.erase(
			vert_indices.end() - std::min<int>(vert_indices.size(), num_keep),
			vert_indices.end());
		// sort such those last in the graph vertex list come first
		std::sort(vert_indices.begin(), vert_indices.end(), std::greater<int>());
		// delete from back to front (this is ok as it does not invalidate the indices!)
		for(int i : vert_indices) {
			boost::clear_vertex(i, strand_graph);
			boost::remove_vertex(i, strand_graph);
		}
	}

	// re-compute dasp_to_strand
	for(auto& posid : dasp_to_strand) {
		posid = -1;
	}
	for(auto vid : as_range(boost::vertices(strand_graph))) {
		int posid = strand_graph[vid].last_superpixel_posid;
		if(posid != -1) {
			dasp_to_strand[posid] = vid;
		}
	}
	for(auto posid : dasp_to_strand) {
		assert(posid != -1);
	}

	return dasp_to_strand;
}

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

SegmentationParameters::SegmentationParameters()
: num_ev(32)
{}

graphseg::SpectralGraph segment_strand_graph(const StrandGraph& strand_graph, const SegmentationParameters& params)
{
	// computing spectral graph and labels
	graphseg::SpectralGraph input_graph;
	boost::copy_graph(strand_graph, input_graph,
		boost::edge_copy([&strand_graph,&input_graph](StrandGraph::edge_descriptor src, typename graphseg::SpectralGraph::edge_descriptor& dst) {
			input_graph[dst] = strand_graph[src].weight;
		}));
	return graphseg::SolveSpectral(input_graph, params.num_ev);
}

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

UcmParameters::UcmParameters()
: t(8.0f),
  t_join(2.0f),
  t_separate(12.0f)
{}

std::vector<int> label_graph(const StrandGraph& strand_graph, const graphseg::SpectralGraph& spectral_graph, const UcmParameters& params, graphseg::impl::ucm_sd_debug* dbg)
{
	// std::vector<int> strand_labeling
	// = graphseg::ComputeSegmentLabels_UCM_Supervised(
	// 	spectral_graph,
	// 	boost::get(boost::edge_bundle, spectral_graph),
	// 	boost::get(&Strand::segment_label, strand_graph),
	// 	params.t);
	std::vector<int> strand_labeling
	= graphseg::ComputeSegmentLabels_UCM_Supervised_Dynamic(
		spectral_graph,
		boost::get(boost::edge_bundle, spectral_graph),
		boost::get(&Strand::segment_label, strand_graph),
		params.t_join, params.t, params.t_separate,
		dbg);
	// // set strand label
	// for(auto vid : as_range(boost::vertices(strand_graph))) {
	// 	strand_graph[vid].segment_label = strand_labeling[vid];
	// }
	return strand_labeling;
}

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

}
