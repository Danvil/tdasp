#ifndef STRANDDASV_STRANDDASV_HPP
#define STRANDDASV_STRANDDASV_HPP

#include <rgbd/rgbd.hpp>
#include <dasp/Superpixels.hpp>
#include <dasp/Neighbourhood.hpp>
#include <graphseg/Common.hpp>
#include <Slimage/Slimage.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <Eigen/Dense>
#include <vector>
namespace graphseg { namespace impl { struct ucm_sd_debug; } }

namespace strand_dasv {

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //
// assignment of pixels to superpixels

struct DaspParameters
{
	DaspParameters();
	unsigned int count;
	float base_radius;
	unsigned int iterations;
	float weight_spatial;
	float weight_color;
	float weight_normal;
	bool smooth_density;
};

dasp::Superpixels dasp_init(const Rgbd& rgbd, DaspParameters& params);

void dasp(const Rgbd& rgbd, dasp::Superpixels& clustering, const DaspParameters& params);

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //
// cluster-cluster graph weights

struct DaspVertex
{
	dasp::Point point;
	int seed_id;
};

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
	DaspVertex,
	float
> DaspGraph;

struct DaspGraphParameters
{
	DaspGraphParameters();
	float weight_spatial;
	float weight_color;
	float weight_normal;
};

std::pair<DaspGraph, dasp::NeighbourhoodGraph> dasp_graph(const dasp::Superpixels& clustering, const DaspGraphParameters& params);

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //
// strand graph construction

/** Supervoxel strand (vertex in strand graph) */
struct Strand
{
	int strand_id;
	int segment_label;
	int last_superpixel_posid;
	int start_time;
	std::vector<dasp::Point> centers;
	dasp::Point running_mean;

	int length() const {
		return centers.size();
	}
	int end_time() const {
		return start_time + centers.size();
	}
};

/** Supervoxel strand graph edge properties */
struct StrandEdge
{
	float weight;
	std::map<int, float> all_weights;
};

/** Supervoxel strand graph type */
typedef boost::adjacency_list<
	boost::vecS, boost::vecS, boost::undirectedS,
	Strand,
	StrandEdge
> StrandGraph;

/** Parameters for supervoxel strand graph creation from dasp */
struct StrandGraphParameters
{
	StrandGraphParameters();
	float color_threshold;
	float depth_threshold;
	float edge_weight_decay;
	float vertex_mix;
	float edge_weight_mix;
	unsigned int max_num_vertices;
};

std::vector<StrandGraph::vertex_descriptor> dasp_to_strand_graph(StrandGraph& strand_graph, const DaspGraph& dasp, unsigned int t, const StrandGraphParameters& params);

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //
// segmentation of strand graph

struct SegmentationParameters
{
	SegmentationParameters();
	unsigned int num_ev;
};

graphseg::SpectralGraph segment_strand_graph(const StrandGraph& strand_graph, const SegmentationParameters& params);

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //
// thresholding of strand graph to compute segments

struct UcmParameters
{
	UcmParameters();
	float t;
	float t_join;
	float t_separate;
};

std::vector<int> label_graph(const StrandGraph& strand_graph, const graphseg::SpectralGraph& spectral_graph, const UcmParameters& params, graphseg::impl::ucm_sd_debug* dbg);

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

struct Parameters
{
	DaspParameters dasp;
	DaspGraphParameters dasp_graph;
	StrandGraphParameters strand_graph;
	SegmentationParameters segs;
	UcmParameters ucm;
};

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

}
#endif
