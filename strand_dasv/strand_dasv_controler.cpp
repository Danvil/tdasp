#include "strand_dasv_controler.hpp"
#include <common/color.hpp>
#include <dasp/Neighbourhood.hpp>
#include <dasp/Segmentation.hpp>
#include <dasp/Metric.hpp>
#include <dasp/Plots.hpp>
#include <graphseg/graphseg.hpp>
#include <graphseg/Labeling.hpp>
#include <density/Visualization.hpp>
#include <density/PointDensity.hpp>
#include <Slimage/Paint.hpp>
#include <boost/format.hpp>
#include <fstream>
#include <random>
#include <map>

namespace strand_dasv {

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

constexpr float DEBUG_DASP_EDGES = false;
constexpr float DEBUG_DASP_POINTS = true;
constexpr float ENABLE_RANDOM_COLORS = true;
constexpr float DEBUG_LABEL_EDGES = false;

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

StrandDasvControlerImpl::StrandDasvControlerImpl()
{

}

data_ptr StrandDasvControlerImpl::request(unsigned int ri)
{
	if(ri <= data_.size()) {
		data_.resize(ri+1);
	}

	if(ri >= 2) {
		data_[ri-2].reset();
	}
	data_ptr& p = data_[ri];
	if(!p) {
		p = std::make_shared<Data>();
		p->frame = ri;
		if(ri > 0) {
			p->superpixels = data_[ri-1]->superpixels;
			p->strand_graph = data_[ri-1]->strand_graph;
		}
		update(p);
	}

	// for(unsigned int i=0; i<=ri; ++i) {
	// 	data_ptr& p = data_[i];
	// 	if(!p) {
	// 		p = std::make_shared<Data>();
	// 		p->frame = i;
	// 		if(i > 0) {
	// 			p->superpixels = data_[i-1]->superpixels;
	// 			p->strand_graph = data_[i-1]->strand_graph;
	// 		}
	// 		update(p);
	// 	}
	// 	assert(p->frame == i);
	// }
	return data_[ri];
}

void StrandDasvControlerImpl::update(const data_ptr& x)
{
	// load frame
	x->rgbd = rgbd_fnc_(x->frame);
	std::cout << "DASV: t=" << x->frame << std::flush;
	// create plot
	if(plot_fnc_) {
		plot_fnc_("color", x->rgbd.color);
		slimage::Image3ub vis_depth = common::GreyDepth(x->rgbd.depth, 500, 3000);
		plot_fnc_("depth", vis_depth);
	}
	// update rest
	update(x, params.dasp);
}

void StrandDasvControlerImpl::update(const data_ptr& x, const DaspParameters& p)
{
	params.dasp = p;
	dasp::Superpixels& sp = x->superpixels;
	std::vector<Eigen::Vector2f> pnts_prev = sp.getClusterCentersAsPoints();
	// compute dasp
	if(x->frame == 0) {
		x->superpixels = dasp_init(x->rgbd, params.dasp);
		std::cout << ", requested=" << sp.opt.count << " -> r=" << sp.opt.base_radius << std::flush;
	}
	else {
		dasp(x->rgbd, sp, params.dasp);
		std::cout << ", num_dasp=" << sp.clusterCount() << std::flush;
	}
	// create dasp plot
	if(plot_fnc_) {
		const unsigned int width = sp.width();
		const unsigned int height = sp.height();
		slimage::Image3ub vis_dasp(width, height, {{0,0,0}});
		dasp::plots::PlotClusters(vis_dasp, sp, dasp::plots::ClusterPoints, dasp::plots::Color);
		if(DEBUG_DASP_EDGES) {
			dasp::plots::PlotEdges(vis_dasp, sp.ComputeLabels(), slimage::Pixel3ub{{255,255,255}}, 2);
		}
		if(DEBUG_DASP_POINTS) {
			dasp::plots::PlotClusters(vis_dasp, sp,
				dasp::plots::ClusterMode::ClusterCenterBig, dasp::plots::ColorMode::UniBlack);
		}
		plot_fnc_("dasp", vis_dasp);
	}
	// create density plot
	if(plot_fnc_) {
		float d_a = 0.90f*sp.density.minCoeff();
		float d_b = 1.10f*sp.density.maxCoeff();
		float d_e = 0.20f/1.10f * d_b;
		slimage::Image3ub vis_density = density::PlotDensity(sp.density, d_a, d_b);
		plot_fnc_("density", vis_density);
		std::vector<Eigen::Vector2f> pnts = sp.getClusterCentersAsPoints();
		Eigen::MatrixXf seed_density = density::PointDensity(pnts, sp.density);
		slimage::Image3ub vis_cluster_density = density::PlotDensity(seed_density, d_a, d_b);
		plot_fnc_("cluster_density", vis_cluster_density);
		slimage::Image3ub vis_dd = density::PlotDeltaDensity(seed_density - sp.density, d_e);
		plot_fnc_("dd", vis_dd);
		// plot cluster movement
		slimage::Image3ub vis_dds(vis_dd.width(), vis_dd.height(), {{255,255,255}});
		{
			// paint old cluster as if deleted
			for(const Eigen::Vector2f& p : pnts_prev) {
				slimage::FillBox(vis_dds,
					std::round(p[0])-1, std::round(p[1])-1, 3, 3,
					slimage::Pixel3ub{{64,64,255}}
				);
			}
			for(size_t i=0; i<pnts.size(); ++i) {
				const Eigen::Vector2f& p = pnts[i];
				const int px = std::round(p[0]);
				const int py = std::round(p[1]);
				int label = sp.cluster[i].seed_id;
				slimage::Pixel3ub color;
				if(label != -1) {
					// cluster was moved
					const int nx = std::round(pnts_prev[label][0]);
					const int ny = std::round(pnts_prev[label][1]);
					// old cluster position
					slimage::FillBox(vis_dds,
						nx-1, ny-1, 3, 3,
						slimage::Pixel3ub{{128,128,128}}
					);
					// connection line
					slimage::PaintLine(vis_dds, px, py, nx, ny,
						slimage::Pixel3ub{{64,64,64}}
					);
					// new cluster position
					slimage::FillBox(vis_dds,
						px-1, py-1, 3, 3,
						slimage::Pixel3ub{{0,0,0}}
					);
				}
				else {
					// new cluster
					slimage::FillBox(vis_dds,
						px-1, py-1, 3, 3,
						slimage::Pixel3ub{{255,64,64}}
					);
				}
			}
		}
		plot_fnc_("dds", vis_dds);
		// plot insertion points
		static slimage::Image3ub vis_insert(640, 480, {{0,0,0}});
		for(size_t i=0; i<pnts.size(); i++) {
			const Eigen::Vector2f& p = pnts[i];
			const int px = std::round(p[0]);
			const int py = std::round(p[1]);
			int label = sp.cluster[i].seed_id;
			slimage::Pixel3ub color;
			if(label == -1) {
				// unsigned char g = std::min<int>(static_cast<int>(vis_insert(px,py)[0])+10,255);
				unsigned char g = 255;
				vis_insert(px,py) = slimage::Pixel3ub{{g,g,g}};
			}
		}
		plot_fnc_("dds_insert", vis_insert);
	}
	// update rest 
	update(x, params.dasp_graph);
}

void StrandDasvControlerImpl::update(const data_ptr& x, const DaspGraphParameters& p)
{
	params.dasp_graph = p;
	// compute dasp graph
	auto q = dasp_graph(x->superpixels, params.dasp_graph);
	x->dasp_graph = q.first;
	x->dasp_nb_graph = q.second;
	// update rest
	update(x, params.strand_graph);
}

template<typename WeightGraph, typename WeightMap>
slimage::Image3ub plot_strand_graph(const dasp::Superpixels& clustering, const StrandGraph& sg, unsigned int t, const WeightGraph& wg, WeightMap weights)
{
	const unsigned int width = clustering.width();
	const unsigned int height = clustering.height();
	slimage::Image3ub vis_dasv_graph(width, height, {{128,128,128}});
	for(auto eid : as_range(boost::edges(sg))) {
		auto ia = boost::source(eid, sg);
		auto ib = boost::target(eid, sg);
		auto q = boost::edge(ia, ib, wg);
		assert(q.second);
		float weight = weights[q.first];
		if(sg[ia].end_time() == t+1 && sg[ib].end_time() == t+1) {
			Eigen::Vector2f pa = clustering.opt.camera.project(sg[ia].running_mean.position);
			Eigen::Vector2f pb = clustering.opt.camera.project(sg[ib].running_mean.position);
			slimage::PaintLine(vis_dasv_graph,
				static_cast<int>(pa.x()), static_cast<int>(pa.y()),
				static_cast<int>(pb.x()), static_cast<int>(pb.y()),
				dasp::plots::IntensityColor(weight, 0, 1)
			);
		}
	}
	return vis_dasv_graph;
}

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
	boost::no_property,	float
> WeightedGraph;

template<typename WeightedStrandGraph, typename WeightMap>
std::map<dasp::NeighbourhoodGraph::edge_descriptor,float>
get_dasp_weights_from_strands(
	const dasp::NeighbourhoodGraph& dasp_nb_graph,
	const std::vector<StrandGraph::vertex_descriptor>& dasp_to_strand_map,
	const WeightedStrandGraph& weighted_strand_graph,
	const WeightMap& weights
) {
	std::map<dasp::NeighbourhoodGraph::edge_descriptor,float> wm;
	for(auto eid : as_range(boost::edges(dasp_nb_graph))) {
		auto va = boost::source(eid, dasp_nb_graph);
		auto vb = boost::target(eid, dasp_nb_graph);
		auto sa = dasp_to_strand_map[va];
		auto sb = dasp_to_strand_map[vb];
		auto q = boost::edge(sa, sb, weighted_strand_graph);
		assert(q.second);
		float w = weights[q.first];
		wm[eid] = w;
	}
	return wm;
}

template<typename WeightMap, typename ColorFunc>
slimage::Image3ub plot_ucm_impl(const dasp::Superpixels& clustering, const dasp::NeighbourhoodGraph& gnb, WeightMap wm, float val_full, float val_empty, ColorFunc cf)
{
	const unsigned int width = clustering.width();
	const unsigned int height = clustering.height();
	Eigen::MatrixXf imgf = Eigen::MatrixXf::Constant(width, height, val_full);
	dasp::PlotBorderPixels(imgf, gnb, wm,
		boost::get(&dasp::NeighbourhoodProperties::border_pixel_ids, gnb));
	for(unsigned int i=0; i<clustering.points.size(); i++) {
		if(!clustering.points[i].is_valid) {
			imgf.data()[i] = val_empty;
		}
	}
	return common::MatrixToImage(imgf, cf);
}

template<typename WeightMap>
slimage::Image3ub plot_ucm_dist(const dasp::Superpixels& clustering, const dasp::NeighbourhoodGraph& gnb, WeightMap wm, float dmax)
{
	return plot_ucm_impl(clustering, gnb, wm, 0.0f, 100.0f*dmax,
		std::bind(&common::InvIntensityColor, std::placeholders::_1, 0.0f, dmax));
}

template<typename WeightMap>
slimage::Image3ub plot_ucm_sim(const dasp::Superpixels& clustering, const dasp::NeighbourhoodGraph& gnb, WeightMap wm)
{
	return plot_ucm_impl(clustering, gnb, wm, 1.0f, 0.0f,
		std::bind(&common::IntensityColor, std::placeholders::_1, 0.0f, 1.0f));
}

void write_strand_dasp_ids(const std::string& fn, const data_ptr& x)
{
	boost::format fmt(fn + "%05d.tsv");
	std::ofstream ofs((fmt % x->frame).str());
	for(std::size_t i=0; i<x->dasp_to_strand_map.size(); ++i) {
		ofs << i << "\t"
			<< x->superpixels.cluster[i].center.px << "\t"
			<< x->superpixels.cluster[i].center.py << "\t"
			<< x->dasp_to_strand_map[i] << std::endl;
	}
}

void StrandDasvControlerImpl::update(const data_ptr& x, const StrandGraphParameters& p)
{
	params.strand_graph = p;
	// update strand graph
	x->dasp_to_strand_map = dasp_to_strand_graph(x->strand_graph, x->dasp_graph, x->frame, params.strand_graph);
	std::cout << ", num_strands=" << boost::num_vertices(x->strand_graph) << std::flush;
//	write_strand_dasp_ids("strand_dasp_", x);
	// create plot
	if(plot_fnc_) {
		// // DASV cluster-cluster metric graph plot on current DASP
		// slimage::Image3ub vis_dasv_graph = plot_strand_graph(
		// 	x->superpixels, x->strand_graph, x->frame,
		// 	x->strand_graph, boost::get(&StrandEdge::weight, x->strand_graph));
		// plot_fnc_("graph_metric", vis_dasv_graph);
		// DASV cluster-cluster metric UCM plot on current DASP
		auto wm = get_dasp_weights_from_strands(x->dasp_nb_graph, x->dasp_to_strand_map,
			x->strand_graph, boost::get(&StrandEdge::weight, x->strand_graph));
		slimage::Image3ub vis_ucm = plot_ucm_sim(x->superpixels, x->dasp_nb_graph, wm);
		plot_fnc_("ucm_metric", vis_ucm);
	}
	// update rest
	update(x, params.segs);
}

void StrandDasvControlerImpl::update(const data_ptr& x, const SegmentationParameters& p)
{
	params.segs = p;
	// segment graph
	x->spectral_graph = segment_strand_graph(x->strand_graph, params.segs);
	std::cout << ", spectral" << std::flush;
	// create plot
	if(plot_fnc_) {
		// // DASV spectral graph plot on current DASP
		// slimage::Image3ub vis_spectral_graph = plot_strand_graph(
		// 	x->superpixels, x->strand_graph, x->frame,
		// 	x->spectral_graph, boost::get(boost::edge_bundle, x->spectral_graph));
		// plot_fnc_("graph_spectral", vis_spectral_graph);
		// DASV spectral UCM plot on current DASP
		auto wm = get_dasp_weights_from_strands(x->dasp_nb_graph, x->dasp_to_strand_map,
			x->spectral_graph, boost::get(boost::edge_bundle, x->spectral_graph));
		slimage::Image3ub vis_ucm = plot_ucm_dist(x->superpixels, x->dasp_nb_graph, wm, 10.0f);
		plot_fnc_("ucm_spectral", vis_ucm);
	}
	// update rest
	update(x, params.ucm);
}

Eigen::Vector3f RandomColor()
{
	static int col_hack = 0;
	static std::mt19937 e;
	switch(col_hack++) {
		case 0: return {1,0,0};
		case 1: return {0,1,0};
		case 2: return {0,0,1};
		case 3: return {1,1,0};
		case 4: return {0,1,1};
		case 5: return {1,0,1};
		default: {
			std::uniform_real_distribution<float> q(0.15f, 0.85f);
			return { q(e), q(e), q(e) };
		}
	}
}

void PlotEdges(
	const slimage::Image3ub& img,
	const slimage::Pixel3ub& color,
	const std::vector<graphseg::impl::Edge>& edges,
	const std::map<StrandGraph::vertex_descriptor, unsigned int>& strand_to_dasp_map,
	const dasp::NeighbourhoodGraph& nbg
) {
	for(const auto& ed : edges) {
		auto itend = strand_to_dasp_map.end();
		auto ita = strand_to_dasp_map.find(ed.a);
		auto itb = strand_to_dasp_map.find(ed.b);
		if(ita == itend || itb == itend) {
			continue;
		}
		auto e = boost::edge(ita->second, itb->second, nbg);
		if(!e.second) {
			continue;
		}
		std::vector<unsigned int> v_pid = nbg[e.first].border_pixel_ids;
		for(unsigned int pid : v_pid) {
			img[pid] = color;
		}
	}
}

void StrandDasvControlerImpl::update(const data_ptr& x, const UcmParameters& p)
{
	params.ucm = p;
	// compute labels
	graphseg::impl::ucm_sd_debug dbg;
	x->labels = label_graph(x->strand_graph, x->spectral_graph, params.ucm, &dbg);
	assert(x->labels.size() == boost::num_vertices(x->strand_graph));
	for(auto vid : as_range(boost::vertices(x->strand_graph))) {
		x->strand_graph[vid].segment_label = x->labels[vid];
	}
	std::cout << ", labels" << std::endl;
	// create plot
	if(plot_fnc_) {
		const unsigned int width = x->superpixels.width();
		const unsigned int height = x->superpixels.height();
		// compute map for superpixel to strand
		std::vector<int> dasp_pos_id_to_strand_vid(x->superpixels.cluster.size(), -1);
		for(auto vid : as_range(boost::vertices(x->strand_graph))) {
			int posid = x->strand_graph[vid].last_superpixel_posid;
			if(posid != -1) {
				dasp_pos_id_to_strand_vid[posid] = vid;
			}
		}
		// compute cluster labeling (mainly for visualization)
		std::vector<int> superpixel_labeling(x->superpixels.cluster.size());
		for(size_t i=0; i<x->superpixels.cluster.size(); i++) {
			int strand_id = dasp_pos_id_to_strand_vid[i];
			assert(strand_id != -1);
			superpixel_labeling[i] = x->strand_graph[strand_id].segment_label;
		}
		// prepare colors
		static std::map<int,Eigen::Vector3f> label_colors_;
		if(ENABLE_RANDOM_COLORS) {
			for(auto vid : as_range(boost::vertices(x->strand_graph))) {
				int label = x->strand_graph[vid].segment_label;
				if(label_colors_.find(label) == label_colors_.end()) {
					label_colors_[label] = RandomColor();
				}
			}
		}
		else {
			throw 0;
			// std::map<int,ColorComputationMajority> label_colors_all;
			// for(auto vid : as_range(boost::vertices(x->strand_graph))) {
			// 	const auto& s = x->strand_graph[vid];
			// 	int label = s.segment_label;
			// 	if(label != -1) {
			// 		label_colors_all[label].add(s.running_mean.color);
			// 	}
			// }
			// std::map<int,Eigen::Vector3f> label_colors;
			// for(auto& p : label_colors_all) {
			// 	label_colors[p.first] = p.second.get();
			// }
			// label_colors_ = label_colors;
		}
		label_colors_[-1] = { Eigen::Vector3f(1.0f, 1.0f, 1.0f) };
		label_colors_[-2] = { Eigen::Vector3f(0.0f, 0.0f, 0.0f) };
		// cluster label image
		slimage::Image3ub vis_labels(width, height, slimage::Pixel3ub{{0,0,0}});
		slimage::Image1i vis_px_labels(width, height);
		vis_px_labels.fill({-1});
		x->superpixels.ForPixelClusters(
			[&vis_labels,&label_colors_,&superpixel_labeling,&vis_px_labels](unsigned int cid, const dasp::Cluster& c, unsigned int pid, const dasp::Point& p) {
				if(cid != -1) {
					int label = superpixel_labeling[cid];
					vis_labels[pid] = common::ColorToPixel(label_colors_[label]);
					vis_px_labels[pid] = label;
				}
			});
		if(DEBUG_LABEL_EDGES) {
			// cluster label edges
			for(int y=1; y<height-1; y++) {
				for(int x=1; x<width-1; x++) {
					int cid = vis_px_labels(x,y);
					if(    cid != vis_px_labels(x,y-1)
						|| cid != vis_px_labels(x-1,y)
						|| cid != vis_px_labels(x,y+1)
						|| cid != vis_px_labels(x+1,y)
					) {
						const slimage::Pixel3ub& v = vis_labels(x,y);
						// unsigned char cr = 255 - v[0];
						// unsigned char cg = 255 - v[1];
						// unsigned char cb = 255 - v[2];
						unsigned char cr = 255;
						unsigned char cg = 255;
						unsigned char cb = 255;
						vis_labels(x,y) = {{cr, cg, cb}};
					}
				}
			}
		}
		plot_fnc_("segs_labels", vis_labels);
		// visualize ucm label merging
		// slimage::Image3ub vis_ucm_merge(width, height, {{255,255,255}});
		slimage::Image3ub vis_ucm_merge = vis_labels.clone();
		for(int i=0; i<vis_ucm_merge.size(); i++) {
			slimage::Pixel3ub col = vis_ucm_merge[i];
			col[0] = static_cast<int>(0.12f*static_cast<float>(col[0]) + 0.88f*255.0f);
			col[1] = static_cast<int>(0.12f*static_cast<float>(col[1]) + 0.88f*255.0f);
			col[2] = static_cast<int>(0.12f*static_cast<float>(col[2]) + 0.88f*255.0f);
			vis_ucm_merge[i] = col;
		}
		std::map<StrandGraph::vertex_descriptor, unsigned int> strand_to_dasp_map;
		for(size_t i=0; i<x->dasp_to_strand_map.size(); i++) {
			strand_to_dasp_map[x->dasp_to_strand_map[i]] = i;
		}
		PlotEdges(vis_ucm_merge, {{210,210,210}}, dbg.force_merge, strand_to_dasp_map, x->dasp_nb_graph);
		PlotEdges(vis_ucm_merge, {{255,128,0}}, dbg.normal_merge, strand_to_dasp_map, x->dasp_nb_graph);
		PlotEdges(vis_ucm_merge, {{0,0,192}}, dbg.reject_merge, strand_to_dasp_map, x->dasp_nb_graph);
		PlotEdges(vis_ucm_merge, {{64,192,64}}, dbg.keep_merge, strand_to_dasp_map, x->dasp_nb_graph);
		PlotEdges(vis_ucm_merge, {{0,12,64}}, dbg.no_merge, strand_to_dasp_map, x->dasp_nb_graph);
		for(unsigned int i=0; i<x->superpixels.points.size(); i++) {
			if(!x->superpixels.points[i].is_valid) {
				vis_ucm_merge[i] = {{0,0,0}};
			}
		}
		plot_fnc_("ucm_merge", vis_ucm_merge);
	}
}

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

}
