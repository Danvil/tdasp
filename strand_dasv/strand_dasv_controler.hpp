#ifndef STRANDDASV_STRANDDASV_CONTROLER_HPP
#define STRANDDASV_STRANDDASV_CONTROLER_HPP

#include "strand_dasv.hpp"
#include <rgbd/rgbd.hpp>
#include <dasp/Superpixels.hpp>
#include <dasp/Neighbourhood.hpp>
#include <graphseg/Common.hpp>
#include <Slimage/Slimage.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <Eigen/Dense>
#include <functional>
#include <memory>
#include <vector>

namespace strand_dasv {

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

struct Data
{
	unsigned int frame;
	Rgbd rgbd;
	dasp::Superpixels superpixels;
	dasp::NeighbourhoodGraph dasp_nb_graph;
	DaspGraph dasp_graph;
	std::vector<StrandGraph::vertex_descriptor> dasp_to_strand_map;
	StrandGraph strand_graph;
	graphseg::SpectralGraph spectral_graph;
	std::vector<int> labels;
};

typedef std::shared_ptr<Data> data_ptr;

struct StrandDasvControlerImpl
{
	typedef std::function<Rgbd(unsigned int)> rgbd_fnc_type;
	typedef std::function<void(const std::string&, const slimage::Image3ub&)> plot_fnc_type;

	StrandDasvControlerImpl();

	void setRgbdFunction(rgbd_fnc_type f) {
		rgbd_fnc_ = f;
	}

	void setPlotFunction(plot_fnc_type f) {
		plot_fnc_ = f;
	}

	unsigned int getCurrentFrameId() const {
		return (data_.size() == 0) ? 0 : (data_.size() - 1);
	}

	data_ptr request(unsigned int i);

	data_ptr requestCurrent() {
		request(getCurrentFrameId());
	}

	Parameters params;

private:
	void update(const data_ptr& x);
	void update(const data_ptr& x, const DaspParameters& p);
	void update(const data_ptr& x, const DaspGraphParameters& p);
	void update(const data_ptr& x, const StrandGraphParameters& p);
	void update(const data_ptr& x, const SegmentationParameters& p);
	void update(const data_ptr& x, const UcmParameters& p);

private:
	rgbd_fnc_type rgbd_fnc_;

	bool dirty_dasp_;
	bool dirty_dasp_graph_;

	std::vector<data_ptr> data_;

	plot_fnc_type plot_fnc_;

};

// ~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~ //

}
#endif
