#include "WdgtSettings.h"

WdgtSettings::WdgtSettings(QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);

	QObject::connect(ui.horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(OnGuiChange()));
	QObject::connect(ui.doubleSpinBoxSimColSig, SIGNAL(valueChanged(double)), this, SLOT(OnGuiChange()));
	QObject::connect(ui.doubleSpinBoxSimPosSig, SIGNAL(valueChanged(double)), this, SLOT(OnGuiChange()));
	QObject::connect(ui.doubleSpinBoxSimNormSig, SIGNAL(valueChanged(double)), this, SLOT(OnGuiChange()));
	QObject::connect(ui.doubleSpinBoxMotion, SIGNAL(valueChanged(double)), this, SLOT(OnGuiChange()));
	QObject::connect(ui.doubleSpinBoxMergeThreshold, SIGNAL(valueChanged(double)), this, SLOT(OnGuiChange()));
	QObject::connect(ui.pushButtonRun, SIGNAL(clicked()), this, SLOT(OnRun()));
	OnGuiChange();
}

WdgtSettings::~WdgtSettings()
{

}

void WdgtSettings::OnGuiChange()
{
	p_slider_ = ui.horizontalSlider->value();
	p_sim_col_sig_ = ui.doubleSpinBoxSimColSig->value();
	p_sim_pos_sig_ = ui.doubleSpinBoxSimPosSig->value();
	p_sim_norm_sig_ = ui.doubleSpinBoxSimNormSig->value();
	p_motion_ = ui.doubleSpinBoxMotion->value();
	p_merge_threshold_ = ui.doubleSpinBoxMergeThreshold->value();
}

void WdgtSettings::OnRun()
{
	if(on_run_) {
		on_run_();
	}
}
