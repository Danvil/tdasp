#ifndef TDASP_WDGTSETTINGS_H
#define TDASP_WDGTSETTINGS_H

#include <QtGui/QWidget>
#include "ui_WdgtSettings.h"
#include <functional>

class WdgtSettings : public QWidget
{
    Q_OBJECT

public:
	WdgtSettings(QWidget *parent = 0);
	~WdgtSettings();

	float p_sim_col_sig_;
	float p_sim_pos_sig_;
	float p_sim_norm_sig_;
	float p_motion_;
	float p_merge_threshold_;

	unsigned int p_slider_;

	std::function<void()> on_run_;

public Q_SLOTS:
	void OnGuiChange();
	void OnRun();

public:
    Ui::WdgtSettingsClass ui;
};

#endif
