#include "tdasp.hpp"
#include "WdgtSettings.h"
#include <common/WdgtMain.h>
#include <QtGui>
#include <QApplication>
#include <boost/program_options.hpp>
#include <boost/timer.hpp>
#include <string>

int main(int argc, char *argv[])
{
	namespace po = boost::program_options;

	std::string p_tag = "/tmp/";

	// parse command line options
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("tag", po::value(&p_tag), "path/tag for output file names")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);    

	if (vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	std::cout << "Loading frames ..." << std::endl;
	boost::timer timer;
	std::vector<dasp::DaspGraph> v_dasp_graphs = dasp::tdasp::LoadGraphStream(p_tag);
	std::cout << "Finished (loaded " << v_dasp_graphs.size() << " frames in " << timer.elapsed() << "s)" << std::endl;

	QApplication a(argc, argv);

	WdgtMain w;
	
	std::shared_ptr<dasp::tdasp::TemporalDaspControler> client(
		new dasp::tdasp::TemporalDaspControler(v_dasp_graphs));
	w.setClient(client);

	w.show();

	return a.exec();
}
