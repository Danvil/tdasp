#include "tdasp.hpp"
#include <common/color.hpp>
#include <graphseg/Rendering.hpp>
#include <graphseg/graphseg.hpp>
#include <graphseg/Labeling.hpp>
#include <dasp/IO.hpp>
#include <boost/format.hpp>
#include <boost/timer.hpp>
#include <boost/graph/copy.hpp>
#include <random>
#include <iostream>
#include <ctime>

namespace dasp {
namespace tdasp {

static std::mt19937 e;

constexpr float BASE_RADIUS = 0.03f;

float SIMILARITY_COLOR_SIGMA = 0.1f;
float SIMILARITY_POSITION_SIGMA = 1.0f*BASE_RADIUS;
float SIMILARITY_NORMAL_SIGMA = 0.1f;
float MOTION_THRESHOLD = 0.1f;
float GRAPHSEG_CUT_THRESHOLD = 5.0f;

std::vector<DaspGraph> LoadGraphStream(const std::string& tag)
{
	boost::format fmt_vertices(tag + "%05d_vertices.tsv");
	boost::format fmt_edges(tag + "%05d_edges.tsv");
	std::vector<DaspGraph> graphs;
	graphs.reserve(120);
	for(unsigned int i=1; true; i++) {
		try {
			auto g = LoadDaspGraph((fmt_vertices%i).str(), (fmt_edges%i).str());
			graphs.push_back(g);
		}
		catch(...) {
			break;
		}
	}
	return graphs;
}

TemporalDaspControler::TemporalDaspControler(const std::vector<DaspGraph>& graphs)
: graphs_(graphs)
{
	e.seed(time(0));

	t_graphs_.resize(graphs_.size() - 1);
	labels_.resize(graphs_.size());

	wdgt_settings_ = std::make_shared<WdgtSettings>();
	wdgt_settings_->show();
	wdgt_settings_->ui.horizontalSlider->setMaximum(graphs.size() - 1);
	wdgt_settings_->on_run_ = std::bind(&TemporalDaspControler::runComputation, this);
	displayed_ = 0;
}

TemporalDaspControler::~TemporalDaspControler()
{
	wdgt_settings_->close();
}

float WeightSameTime(const Point& a, const Point& b)
{
	const float dp = std::max(0.0f, (a.position - b.position).norm() - 2.0f*BASE_RADIUS);
	const float dc2 = (a.color - b.color).squaredNorm();
	return std::exp(-2.0f*0.5f*(
		dp*dp/(SIMILARITY_POSITION_SIGMA*SIMILARITY_POSITION_SIGMA)
		+ dc2/(SIMILARITY_COLOR_SIGMA*SIMILARITY_COLOR_SIGMA)));
}

float WeightDifferentTime(const Point& a, const Point& b, bool& ok)
{
	const float dp2 = (a.position - b.position).squaredNorm();
	ok = (dp2 < MOTION_THRESHOLD * MOTION_THRESHOLD);
	if(!ok) {
		return 0.0f;
	}
	const float dc2 = (a.color - b.color).squaredNorm();
	return std::exp(-2.0f*0.5f*(
		dp2/(MOTION_THRESHOLD*MOTION_THRESHOLD)
		+ dc2/(SIMILARITY_COLOR_SIGMA*SIMILARITY_COLOR_SIGMA)));
}

Graph BuildTemporalConnections(const DaspGraph& g_prev, const DaspGraph& g_next)
{
	Graph::edge_descriptor added_eid;
	bool added_ok;
	// add prev and next vertices and edges
	Graph g(boost::num_vertices(g_prev) + boost::num_vertices(g_next));
	for(auto vid : as_range(boost::vertices(g_prev))) {
		auto& x = g[vid];
		x.time = 0;
		x.label = -1;
		x.point = g_prev[vid];
	}
	for(auto eid : as_range(boost::edges(g_prev))) {
		auto v1 = boost::source(eid, g_prev);
		auto v2 = boost::target(eid, g_prev);
		boost::tie(added_eid, added_ok) = boost::add_edge(v1, v2, g);
		g[added_eid].similarity = WeightSameTime(g_prev[v1], g_prev[v2]);
		g[added_eid].spectral = g[added_eid].similarity;
	}
	const unsigned int offset = boost::num_vertices(g_prev);
	for(auto vid : as_range(boost::vertices(g_next))) {
		auto& x = g[offset + vid];
		x.time = 1;
		x.label = -1;
		x.point = g_next[vid];
	}
	for(auto eid : as_range(boost::edges(g_next))) {
		auto v1 = boost::source(eid, g_next);
		auto v2 = boost::target(eid, g_next);
		boost::tie(added_eid, added_ok) = boost::add_edge(offset + v1, offset + v2, g);
		g[added_eid].similarity = WeightSameTime(g_next[v1], g_next[v2]);
		g[added_eid].spectral = g[added_eid].similarity;
	}
	// temporal connections
	for(auto vn : as_range(boost::vertices(g_next))) {
		const Point& p1 = g_next[vn];
		for(auto vp : as_range(boost::vertices(g_prev))) {
			bool ok;
			const float w = WeightDifferentTime(p1, g_prev[vp], ok);
			if(ok) {
				boost::tie(added_eid, added_ok) = boost::add_edge(offset + vn, vp, g);
				g[added_eid].similarity = w;
				g[added_eid].spectral = w;
			}
		}
	}
	return g;
}

Eigen::Vector3f RandomColor()
{
	std::uniform_real_distribution<float> q(0.15f, 0.85f);
	return { q(e), q(e), q(e) };
}

void TemporalDaspControler::runComputation()
{
	unsigned int k = displayed_;
	Graph g = BuildTemporalConnections(graphs_[k], graphs_[k+1]);

	const unsigned int offset = boost::num_vertices(graphs_[k]);

	// do spectral theory
	std::cout << "Spectral graph segmentation for graph with " << boost::num_vertices(g) << " vertices" << std::endl;
	graphseg::SpectralGraph input_graph;
	boost::copy_graph(g, input_graph,
		boost::edge_copy([&g,&input_graph](Graph::edge_descriptor src, typename graphseg::SpectralGraph::edge_descriptor& dst) {
			input_graph[dst] = g[src].similarity;
		}));
	boost::timer timer;
	graphseg::SpectralGraph sg = graphseg::SolveSpectral(input_graph,
		32, graphseg::SpectralMethod::Eigen);
	std::cout << "Finished in " << timer.elapsed() << " s" << std::endl;
	for(auto eid : as_range(boost::edges(g))) {
		g[eid].spectral = sg[eid];
	}
	// prepare labels
	for(auto vid : as_range(boost::vertices(g))) {
		g[vid].label = -1;
	}
	if(labels_[k].size() > 0) {
		for(unsigned int i=0; i<offset; i++) {
			g[i].label = labels_[k][i];
		}
	}
	// compute labels
	std::vector<int> strand_labeling = graphseg::ComputeSegmentLabels_UCM_Supervised(
		sg,
		boost::get(boost::edge_bundle, sg),
		boost::get(&Vertex::label, g),
		GRAPHSEG_CUT_THRESHOLD);
	for(auto vid : as_range(boost::vertices(g))) {
		g[vid].label = strand_labeling[vid];
	}

	t_graphs_[k] = g;

	std::vector<int> l0(boost::num_vertices(graphs_[k]));
	for(unsigned int i=0; i<l0.size(); ++i) {
		l0[i] = strand_labeling[i];
	}
	labels_[k] = l0;
	std::vector<int> l1(boost::num_vertices(graphs_[k+1]));
	for(unsigned int i=0; i<l1.size(); ++i) {
		l1[i] = strand_labeling[offset + i];
	}
	labels_[k+1] = l1;

	label_colors_[-1] = { Eigen::Vector3f(1.0f, 0.0f, 1.0f) };
	for(auto vid : as_range(boost::vertices(g))) {
		const int label = g[vid].label;
		if(label_colors_.find(label) == label_colors_.end()) {
			label_colors_[label] = RandomColor();
		}
	}

}

Eigen::Vector3f GetPosition(const dasp::Point& p, int t)
{
	return Eigen::Vector3f{
						0.01f*static_cast<float>(p.px - 640),
						0.01f*static_cast<float>(p.py - 240),
						0.03f*static_cast<float>(t)
					};
}

Eigen::Vector3f GetPosition2(const dasp::Point& p, int t)
{
	return Eigen::Vector3f{
						0.01f*static_cast<float>(p.px),
						0.01f*static_cast<float>(p.py - 240),
						0.03f*static_cast<float>(t)
					};
}

void TemporalDaspControler::OnRender()
{
	// glBegin(GL_POINTS);
	// for(unsigned int i=0; i<graphs_.size(); i++) {
	// 	const auto& g = graphs_[i];
	// 	for(auto vid : as_range(boost::vertices(g))) {
	// 		const auto& p = g[vid];
	// 		graphseg::detail::GlColor(p.color);
	// 		graphseg::detail::GlVertex(GetPosition(p, i));
	// 	}
	// }
	// glEnd();

	const Graph& tg = t_graphs_[displayed_];

	graphseg::RenderEdges3D(tg,
		[&tg](const Graph::vertex_descriptor& vid) {
			return GetPosition(tg[vid].point, 10*tg[vid].time);
		},
		[&tg](const Graph::edge_descriptor& eid) {
			return common::IntensityColor(tg[eid].spectral);
		}
	);

	graphseg::RenderEdges3D(tg,
		[&tg](const Graph::vertex_descriptor& vid) {
			return GetPosition2(tg[vid].point, 10*tg[vid].time);
		},
		[&tg,&label_colors_](const Graph::edge_descriptor& eid) -> Eigen::Vector3f {
			auto v1 = boost::source(eid, tg);
			auto v2 = boost::target(eid, tg);
			if(tg[v1].label == tg[v2].label) {
				return label_colors_[tg[v1].label];	
			}
			else {
				return Eigen::Vector3f::Zero();
			}			
		}
	);

	// glBegin(GL_POINTS);
	// glPointSize(5.0f);
	// graphseg::RenderVertices(graph_used_,
	// 	[&graph_used_,&label_colors_](const Graph::vertex_descriptor& vid) {
	// 		graphseg::detail::GlColor(
	// 			label_colors_[graph_used_[vid].label]);
	// 		graphseg::detail::GlVertex(
	// 			GetPosition2(graph_used_[vid].point, 10*graph_used_[vid].time));
	// 	}
	// );
	// glEnd();
}

void TemporalDaspControler::OnTick()
{
	SIMILARITY_COLOR_SIGMA = wdgt_settings_->p_sim_col_sig_;
	SIMILARITY_POSITION_SIGMA = wdgt_settings_->p_sim_pos_sig_;
	SIMILARITY_NORMAL_SIGMA = wdgt_settings_->p_sim_norm_sig_;
	MOTION_THRESHOLD = wdgt_settings_->p_motion_;
	GRAPHSEG_CUT_THRESHOLD = wdgt_settings_->p_merge_threshold_;

	displayed_ = wdgt_settings_->p_slider_;

}

}}
