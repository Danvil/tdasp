#ifndef TDASP_HPP
#define TDASP_HPP

#include "WdgtSettings.h"
#include <common/Client.hpp>
#include <dasp/Graph.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <thread>
#include <vector>
#include <string>
#include <memory>
#include <map>

namespace dasp {
namespace tdasp {

std::vector<DaspGraph> LoadGraphStream(const std::string& tag);

struct Vertex
{
	int time;
	Point point;
	int label;
};

struct Edge
{
	float similarity;
	float spectral;
};

typedef boost::adjacency_list<boost::vecS,boost::vecS,boost::undirectedS,Vertex,Edge> Graph;

struct TemporalDaspControler
: public Client
{
	TemporalDaspControler(const std::vector<DaspGraph>& graphs);

	~TemporalDaspControler();

	void OnRender();

	void OnTick();

	void runComputation();

private:
	std::vector<DaspGraph> graphs_;

	std::vector<std::vector<int>> labels_;

	std::map<int,Eigen::Vector3f> label_colors_;

	std::vector<Graph> t_graphs_;

	int displayed_;

	std::shared_ptr<WdgtSettings> wdgt_settings_;

};

}}

#endif
